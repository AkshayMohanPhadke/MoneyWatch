/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React, {
  useState,
  useEffect
} from 'react';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  useColorScheme,
} from 'react-native';

import {
  Colors
} from 'react-native/Libraries/NewAppScreen';

import AppNavigationContainer from './Components/Navigation/NavigationController';

import { UserContext } from './Contexts/UserContext';

import { onAuthStateChanged } from "firebase/auth";
import { firebaseAuthentication } from "./Firebase/FirebaseAuthentication";

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';
  const [userData, setUserData] = useState(null);

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  onAuthStateChanged(firebaseAuthentication, (user) => {
    console.log('onAuthStateChanged called');
    if (user) {
      console.log('User Info: ', user);
      setUserData(user);
    }
    else {
      setUserData(null);
    }
  });

  return (
    <UserContext.Provider value={{ userData, setUserData }}>
      <AppNavigationContainer>
        <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      </AppNavigationContainer>
    </UserContext.Provider>
  );
};

export default App;
