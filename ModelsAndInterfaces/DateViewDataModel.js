import React from "react";
import { format, formatDistanceToNow, sub, parse } from "date-fns";
import TransactionViewTypeIndex from "../Constants/TransactionViewTypeIndex";

export default class DateViewDataModel {

    static maximumAllowedPastYears = 10;

    selectedViewType: TransactionViewTypeIndex = TransactionViewTypeIndex.day;
    minDate: Date;
    maxDate: Date;
    dayToDisplay: Number;
    weekToDisplay: string;
    weekday: string;
    monthToDisplay: Number;
    yearToDisplay: Number;

    constructor() {
        const subtractedYear = parseInt(format(sub(new Date(), {
            years: DateViewDataModel.maximumAllowedPastYears
        }), 'yyyy'));
        const subtractedYearString = '01/01/' + subtractedYear;
        this.minDate = parse(subtractedYearString, 'dd/MM/yyyy', new Date());
        this.maxDate = new Date();

        this.dayToDisplay = parseInt(format(this.maxDate, 'dd'));
        this.monthToDisplay = parseInt(format(this.maxDate, 'MM'));
        this.yearToDisplay = parseInt(format(this.maxDate, 'yyyy'));
        console.log('this.dayToDisplay: ', this.dayToDisplay, 'this.monthToDisplay: ', this.monthToDisplay, 'this.yearToDisplay: ', this.yearToDisplay);
    }

    getDayString() {
        console.log('this.dayToDisplay: ', parseInt(format(this.maxDate, 'dd')));
        //return this.dayToDisplay.toString();
    } 
    
    getWeekString() {
        console.log('this.weekToDisplay: ');
        //return this.weekToDisplay.toString();
    } 

    getMonthAndYearString() {
        console.log('this.monthToDisplay: ', this.monthToDisplay.toString());
        //return this.monthToDisplay.toString();
    } 

    getYearString() {
        console.log('this.yearToDisplay: ', this.yearToDisplay.toString());
        //return this.yearToDisplay.toString();
    } 
}