import React, { Component } from "react";

class Constants extends Component {
    static categories = [
        { title: 'Transportation', image: 'car' },
        { title: 'Gym', image: 'dumbbell' },
        { title: 'Cinema', image: 'photo-video' },
        { title: 'Groceries', image: 'shopping-basket' },
        { title: 'Gifts', image: 'gifts' },
        { title: 'Cafe', image: 'mug-hot' },
        { title: 'Home', image: 'home' },
        { title: 'Lunch', image: 'taxi' },
        { title: 'Health Checkup', image: 'clinic-medical' },
        { title: 'Hospitalisation', image: 'hospital' },
        { title: 'Credit Card Payment', image: 'credit-card' }
    ];

    static maximumAllowedPastYears = 10;
    static minDateString = '01/01/2010';
}

export default Constants;