
const enum AuthSelectionType {
    login = "LOGIN",
    signUp = "SIGNUP"
}

export default AuthSelectionType;