
const enum TransactionType {
    expense = "Expense",
    income = "Income",
    transfer = "Transfer"
}

export default TransactionType;