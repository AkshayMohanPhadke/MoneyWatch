
const enum TransactionViewType {
    day = "Day",
    week = "Week",
    month = "Month",
    year = "Year"
}

export default TransactionViewType;