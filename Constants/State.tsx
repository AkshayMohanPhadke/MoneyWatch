
const enum StateValue {
    current,
    previous,
    next
}

export default StateValue;