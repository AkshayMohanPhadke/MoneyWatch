
const enum TransactionViewTypeIndex {
    day = 0,
    week = 1,
    month = 2,
    year = 3
}

export default TransactionViewTypeIndex;