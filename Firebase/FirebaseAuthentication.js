import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
//import * as firebase from 'firebase';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAmCr8WXvsY-qTWz5iQADAgjgu2rV0ZyhE",
  authDomain: "moneywatch-5c77a.firebaseapp.com",
  projectId: "moneywatch-5c77a",
  storageBucket: "moneywatch-5c77a.appspot.com",
  messagingSenderId: "853326227326",
  appId: "1:853326227326:web:e481dbdc757b6c5b82f52f",
  measurementId: "${config.measurementId}"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const firebaseAuthentication = getAuth(app);
export const firebaseCloudFirestoreDB = getFirestore(app);