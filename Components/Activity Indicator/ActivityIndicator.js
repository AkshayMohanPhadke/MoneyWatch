import React from "react";
import { 
    View,
    ActivityIndicator,
    StyleSheet
 } from "react-native";

const ActivityIndicatorComponent = ({ shouldShow }) => {

    const styles = StyleSheet.create({
        activityIndicatorView: {
            backgroundColor: '#fff',
            justifyContent: 'center',
            alignContent: 'center',
            borderRadius: 10,
            marginHorizontal: '36%',
            marginVertical: '63%',
            height: 100
        },
        activityIndicator: {

        }
    });

    return (
        <View style={styles.activityIndicatorView}>
            <ActivityIndicator size="large" animating={shouldShow} color="#182C61"></ActivityIndicator>
        </View>
    )
}

export default ActivityIndicatorComponent;