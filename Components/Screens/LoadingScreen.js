import React from "react";
import {
    StyleSheet
} from "react-native";

import { SafeAreaView } from "react-native-safe-area-context";

import LottieView from 'lottie-react-native';

import { onAuthStateChanged } from "firebase/auth";
import { firebaseAuthentication } from "../../Firebase/FirebaseAuthentication";

const LoadingScreenView = ({ navigation }) => {

    onAuthStateChanged(firebaseAuthentication, (user) => {
        console.log('onAuthStateChanged called');
        if (!user) {
          console.log('User Info: ', user);
          navigation.navigate('AuthSelection');
        }
      });

    const styles = StyleSheet.create({
        mainContentView: {
            flex: 1,
            backgroundColor: '#182C61'
        }
    });

    return (
        <SafeAreaView style={styles.mainContentView}>
            <LottieView source={require('../../Assets/Loading-Spinner-Animation.json')} autoPlay loop colorFilters={[
                {
                    keypath: 'Line',
                    color: '#EAB543',
                },
                {
                    keypath: 'Line transparent',
                    color: '#111',
                },
            ]} />
        </SafeAreaView>
    );
}

export default LoadingScreenView;