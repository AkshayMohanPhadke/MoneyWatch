import React, {
    useState,
    useContext
} from "react";
import {
    View,
    Text,
    TextInput,
    StyleSheet,
    TouchableOpacity,
    KeyboardAvoidingView
} from "react-native";

import { SafeAreaView } from "react-native-safe-area-context";

import LottieView from 'lottie-react-native';

import { UserContext } from "../../Contexts/UserContext";

import { collection, doc, setDoc, addDoc, getDoc, getDocs } from "firebase/firestore";
import { firebaseAuthentication, firebaseCloudFirestoreDB } from "../../Firebase/FirebaseAuthentication";

const AddCategoryScreenView = ({ navigation, route }) => {

    const userData = useContext(UserContext);
    const [categoryName, setCategoryName] = useState('');
    const [shouldShowActivityIndicator, setShouldShowActivityIndicator] = useState(false);

    const { categories } = route.params;

    const styles = StyleSheet.create({
        mainContentView: {
            flex: 1,
            backgroundColor: '#182C61',
            justifyContent: 'center'
        },
        setAccountBalanceTextView: {
            backgroundColor: 'transparent',
            //marginTop: '15%'

        },
        setAccountBalanceTextTitle: {
            fontSize: 28,
            fontWeight: 'bold',
            alignSelf: 'center',
            color: '#fff',
            //marginBottom: '10%',
            textAlign: 'center'
        },
        setAccountBalanceTextSubtitle: {
            fontSize: 22,
            fontWeight: '600',
            alignSelf: 'center',
            color: '#fff',
            textAlign: 'center',
            marginHorizontal: '10%'
        },
        accountBalanceInputView: {
            marginTop: '15%',
            marginHorizontal: '10%'
        },
        accountNameInput: {
            marginTop: '15%',
            marginHorizontal: '12%',
            height: 40,
            padding: 10,
            textAlign: 'center',
            color: '#F8EFBA',
            fontSize: 18,
            fontWeight: '700'
        },
        accounNameeInputBottomLineContentView: {
            marginTop: '2%',
            marginHorizontal: '12%',
            height: 1,
            backgroundColor: 'transparent',
            alignItems: 'center'
        },
        accountBalanceInput: {
            marginTop: '15%',
            marginHorizontal: '12%',
            height: 40,
            padding: 10,
            textAlign: 'center',
            color: '#F8EFBA',
            fontSize: 18,
            fontWeight: '700'
        },
        accountBalanceInputBottomLineContentView: {
            marginTop: '2%',
            marginHorizontal: '20%',
            height: 1,
            backgroundColor: 'transparent',
            alignItems: 'center'
        },
        accountBalanceInputBottomLineView: {
            marginTop: '2%',
            marginHorizontal: '10%',
            height: 1,
            backgroundColor: '#F8EFBA',
            width: '100%'
        },
        nextButtonView: {
            marginTop: '15%',
            alignSelf: 'center',
            width: '70%',
            backgroundColor: '#EAB543',
            padding: 20,
            borderRadius: 45
        },
        nextText: {
            fontSize: 18,
            fontWeight: '600',
            alignSelf: 'center',
            color: '#2C3A47'
        }
    });

    const SetAccountBalanceTextComponent = () => {
        return (
            <View style={styles.setAccountBalanceTextView}>
                <Text style={styles.setAccountBalanceTextTitle}>
                    Give the category a name
                </Text>
            </View>
        );
    }

    const AccountBalanceInputComponent = () => {
        return (
            <TextInput
                style={styles.accountBalanceInput}
                selectionColor={'#F8EFBA'}
                value={accountBalance}
                onChangeText={(text) => setAccountBalance(text)}
            />
        );
    }

    const AccountBalanceInputBottomLineComponent = () => {
        return (
            <View style={styles.accountBalanceInputBottomLineContentView}>
                <View style={styles.accountBalanceInputBottomLineView}></View>
            </View>
        );
    }

    const AddCategoryButtonComponent = () => {
        return (
            <TouchableOpacity onPress={addCategoryButtonAction}>
                <View style={styles.nextButtonView}>
                    <Text style={styles.nextText}>Add Category</Text>
                </View>
            </TouchableOpacity>
        );
    }

    const addCategoryButtonAction = () => {
        if (categoryName == '' || categoryName == null) {
            alert('Please give the Category a name.');
        }
        else {
            const duplicateCategoriesArray = categories.filter((item) => {
                return item.title == categoryName;
            });
            if (duplicateCategoriesArray.length > 0) {
                alert('The given Category already exists.');
            }
            else {
                addCategory();
            }
        }
    }

    const addCategory = async () => {
        try {
            if (userData != null) {

                let categoryObject = {
                    title: categoryName,
                    image: 'question-circle',
                    color: generateRandomColor()
                }

                const docRef = doc(firebaseCloudFirestoreDB, "users", userData.userData.uid, "categories", categoryObject.title);
                const setDocRef = setDoc(docRef, categoryObject, { merge: true });
                console.log("categoryObject Document written: ", setDocRef);
                navigation.navigate(
                    'HomeDrawer', {
                    screen: 'Categories',
                    params: {
                        categoryObject: {
                            title: categoryName,
                            image: 'question-circle',
                            color: generateRandomColor()
                        }
                    },
                    merge: true
                }
                );
                alert('Category added successfully!');
            }
            else {
                alert('There was some error in accessing your stored data. Please sign in again to authenticate.');
            }
        }
        catch (error) {
            alert(error.message);
        }
    }

    const generateRandomColor = () => {
        //return '#' + Math.floor(Math.random()*16777215).toString(16).slice(2, 8).toUpperCase().slice(-6);
        //return '#' +  Math.random().toString(16).substring(-6);
        return '#' + ("000000" + Math.random().toString(16).slice(2, 8).toUpperCase()).slice(-6);
    }

    return (
        <SafeAreaView style={styles.mainContentView}>
            <KeyboardAvoidingView>
                <SetAccountBalanceTextComponent />
                <TextInput
                    style={styles.accountBalanceInput}
                    placeholderTextColor={'#F8EFBA'}
                    placeholder="A meaningful name, isn't it?"
                    maxLength={10}
                    keyboardAppearance={'dark'}
                    returnKeyType={'done'}
                    selectionColor={'#F8EFBA'}
                    value={categoryName}
                    onChangeText={(text) => setCategoryName(text)}
                />
                <AccountBalanceInputBottomLineComponent />
                <AddCategoryButtonComponent />
            </KeyboardAvoidingView>
            {(shouldShowActivityIndicator) ? <LottieView source={require('../../Assets/Loading-Spinner-Animation.json')} autoPlay loop colorFilters={[
                {
                    keypath: 'Line',
                    color: '#EAB543',
                },
                {
                    keypath: 'Line transparent',
                    color: '#111',
                },
            ]} /> : null}
        </SafeAreaView>
    );
}

export default AddCategoryScreenView;