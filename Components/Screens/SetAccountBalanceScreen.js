import React, {
    useState,
    useContext,
    useEffect
} from "react";
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    TextInput,
    KeyboardAvoidingView
} from "react-native";

import { SafeAreaView } from "react-native-safe-area-context";

import LottieView from 'lottie-react-native';

import { UserContext } from "../../Contexts/UserContext";

import { collection, doc, setDoc, addDoc, getDoc, getDocs } from "firebase/firestore";
import { firebaseAuthentication, firebaseCloudFirestoreDB } from "../../Firebase/FirebaseAuthentication";

const SetAccountBalanceScreenView = ({ navigation, route }) => {

    const userData = useContext(UserContext);
    const [accounts, setAccounts] = useState([]);
    const [isAddNewAccountMode, setIsAddNewAccountMode] = useState(false);
    const [accountName, setAccountName] = useState('');
    const [accountBalance, setAccountBalance] = useState('');
    const [shouldShowActivityIndicator, setShouldShowActivityIndicator] = useState(false);

    const styles = StyleSheet.create({
        mainContentView: {
            flex: 1,
            backgroundColor: '#182C61',
            justifyContent: 'center'
        },
        setAccountBalanceTextView: {
            backgroundColor: 'transparent',
            //marginTop: '15%'

        },
        setAccountBalanceTextTitle: {
            fontSize: 28,
            fontWeight: 'bold',
            alignSelf: 'center',
            color: '#fff',
            marginBottom: '10%',
            textAlign: 'center'
        },
        setAccountBalanceTextSubtitle: {
            fontSize: 22,
            fontWeight: '600',
            alignSelf: 'center',
            color: '#fff',
            textAlign: 'center',
            marginHorizontal: '10%'
        },
        accountBalanceInputView: {
            marginTop: '15%',
            marginHorizontal: '10%'
        },
        accountNameInput: {
            marginTop: '15%',
            marginHorizontal: '12%',
            height: 40,
            padding: 10,
            textAlign: 'center',
            color: '#F8EFBA',
            fontSize: 18,
            fontWeight: '700'
        },
        accounNameeInputBottomLineContentView: {
            marginTop: '2%',
            marginHorizontal: '12%',
            height: 1,
            backgroundColor: 'transparent',
            alignItems: 'center'
        },
        accountBalanceInput: {
            marginTop: '15%',
            marginHorizontal: '12%',
            height: 40,
            padding: 10,
            textAlign: 'center',
            color: '#F8EFBA',
            fontSize: 18,
            fontWeight: '700'
        },
        accountBalanceInputBottomLineContentView: {
            marginTop: '2%',
            marginHorizontal: '20%',
            height: 1,
            backgroundColor: 'transparent',
            alignItems: 'center'
        },
        accountBalanceInputBottomLineView: {
            marginTop: '2%',
            marginHorizontal: '10%',
            height: 1,
            backgroundColor: '#F8EFBA',
            width: '100%'
        },
        nextButtonView: {
            marginTop: '15%',
            alignSelf: 'center',
            width: '90%',
            backgroundColor: '#EAB543',
            padding: 20,
            borderRadius: 45
        },
        nextText: {
            fontSize: 18,
            fontWeight: '600',
            alignSelf: 'center',
            color: '#2C3A47'
        }
    });

    useEffect(() => {
        getAccounts();
        navigation.setOptions({ headerShown: route.params.headerShown });
        setIsAddNewAccountMode(route.params.isAddNewAccountMode);
    }, []);


    const getAccounts = async () => {
        if (userData != null) {
            try {
                const querySnapshot = await getDocs(collection(firebaseCloudFirestoreDB, "users", userData.userData.uid, "accounts"));
                if (!querySnapshot.empty) {
                    let accountsArray = []
                    querySnapshot.forEach((doc) => {
                        accountsArray.push(doc.data());
                    });
                    setAccounts(accountsArray);
                }
            }
            catch {
                alert(console.error.message);
            }
        }
        else {
            alert('There was some error in accessing your stored data. Please sign in again to authenticate.');
        }
    }

    const SetAccountBalanceTextComponent = () => {
        return (
            <View style={styles.setAccountBalanceTextView}>
                <Text style={styles.setAccountBalanceTextTitle}>
                    Give your account a name and set its balance
                </Text>
                <Text style={styles.setAccountBalanceTextSubtitle}>
                    You can set balance for different accounts later in the Accounts section.
                </Text>
            </View>
        );
    }

    const AccountBalanceInputComponent = () => {
        return (
            <TextInput
                style={styles.accountBalanceInput}
                selectionColor={'#F8EFBA'}
                value={accountBalance}
                onChangeText={(text) => setAccountBalance(text)}
            //autoFocus={true}
            />
        );
    }

    const AccountBalanceInputBottomLineComponent = () => {
        return (
            <View style={styles.accountBalanceInputBottomLineContentView}>
                <View style={styles.accountBalanceInputBottomLineView}></View>
            </View>
        );
    }

    const NextButtonComponent = () => {
        return (
            <TouchableOpacity onPress={nextButtonAction}>
                <View style={styles.nextButtonView}>
                    <Text style={styles.nextText}>{(isAddNewAccountMode) ? 'Add Account' : 'Next'}</Text>
                </View>
            </TouchableOpacity>
        );
    }

    const nextButtonAction = () => {
        if (isAddNewAccountMode) {
            if ((accountName == '' || accountName == null) && (accountBalance == '' || accountBalance == null)) {
                alert('Please fill in all the fields to continue.');
            }
            else if ((accountName != '') && (accountBalance == '' || accountBalance == null)) {
                alert('Please set Balance for the Account.');
            }
            else if ((accountName == '' || accountName == null) && (accountBalance != '')) {
                alert('Please give the Account a Name.');
            }
            else {
                const duplicateAccountNamesArray = accounts.filter((item) => {
                    return item.name == accountName;
                });
                if (duplicateAccountNamesArray.length > 0) {
                    alert('An account with a same name already exists. Please give a unique name to the account.');
                }
                else {
                    addAccounts();
                }
            }
        }
        else {
            addAccounts();
        }
    }

    const addAccounts = async () => {
        setShouldShowActivityIndicator(true);
        try {

            if (userData != null) {
                let name = 'Primary Savings Account';
                if (accountName != null && accountName != '') {
                    name = accountName;
                }

                const account = {
                    name: name,
                    balance: (accountBalance == '') ? 0 : parseFloat(accountBalance),
                    isSelected: (isAddNewAccountMode) ? false : true
                }

                const docRef = doc(firebaseCloudFirestoreDB, "users", userData.userData.uid, "accounts", account.name);
                const setDocRef = setDoc(docRef, account, { merge: true });
                console.log("primaryAccount Document written: ", setDocRef);
                setShouldShowActivityIndicator(false);

                if (isAddNewAccountMode) {
                    navigation.navigate(
                        'HomeDrawer', {
                        screen: 'Accounts',
                        params: {
                            accountObject: account
                        },
                        merge: true
                    }
                    );
                }
                else {
                    navigation.navigate('HomeDrawer');
                }
            }
            else {
                alert('There was some error in accessing your stored data. Please sign in again to authenticate.');
            }
        }
        catch (error) {
            setShouldShowActivityIndicator(false);
            alert(error.message);
        }
    }

    return (
        <SafeAreaView style={styles.mainContentView}>
            <KeyboardAvoidingView>
                <SetAccountBalanceTextComponent />
                <TextInput
                    style={styles.accountBalanceInput}
                    placeholderTextColor={'#F8EFBA'}
                    placeholder="A name for your account?"
                    keyboardAppearance={'dark'}
                    returnKeyType={'done'}
                    selectionColor={'#F8EFBA'}
                    value={accountName}
                    onChangeText={(text) => setAccountName(text)}
                />
                <AccountBalanceInputBottomLineComponent />
                <TextInput
                    style={styles.accountBalanceInput}
                    placeholderTextColor={'#F8EFBA'}
                    placeholder="Some account balance, maybe?"
                    keyboardType={'numeric'}
                    keyboardAppearance={'dark'}
                    returnKeyType={'done'}
                    selectionColor={'#F8EFBA'}
                    value={accountBalance}
                    onChangeText={(text) => setAccountBalance(text)}
                />
                <AccountBalanceInputBottomLineComponent />
                <NextButtonComponent />
            </KeyboardAvoidingView>
            {(shouldShowActivityIndicator) ? <LottieView source={require('../../Assets/Loading-Spinner-Animation.json')} autoPlay loop colorFilters={[
                {
                    keypath: 'Line',
                    color: '#EAB543',
                },
                {
                    keypath: 'Line transparent',
                    color: '#111',
                },
            ]} /> : null}
        </SafeAreaView>
    );
}

export default SetAccountBalanceScreenView;