import React from "react";
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image
} from "react-native";

import { SafeAreaView } from "react-native-safe-area-context";

const WelcomeScreenView = ({ navigation }) => {

    const styles = StyleSheet.create({
        mainContentView: {
            flex: 1,
            backgroundColor: '#182C61',
            justifyContent: 'center'
        },
        moneyWatchView: {
            marginVertical: '13%'
        },
        moneyWatchText: {
            fontSize: 32,
            fontWeight: 'bold',
            alignSelf: 'center',
            color: '#fff',
            marginBottom: '10%'
        },
        moneyWatchLogo: {
            backgroundColor: 'transparent',
            width: 200,
            height: 200,
            marginBottom: '10%',
            alignSelf: 'center'
        },
        moneyWatchSubtitleText: {
            fontSize: 22,
            fontWeight: '600',
            alignSelf: 'center',
            color: '#fff'
        },
        nextButtonView: {
            alignSelf: 'center',
            width: '90%',
            backgroundColor: '#EAB543',
            padding: 20,
            borderRadius: 45
        },
        nextText: {
            fontSize: 18,
            fontWeight: '600',
            alignSelf: 'center',
            color: '#2C3A47'
        }
    });


    const MoneyWatchInfoComponent = () => {
        const subtitleTextLine1 = "Expenses & Income,";
        const subtitleTextLine2 = "in one place!";
        return (
                <View style={styles.moneyWatchView}>
                    <Text style={styles.moneyWatchText}>Money Watch</Text>
                    <Image
                        style={styles.moneyWatchLogo}
                        source={require('../../Assets/MoneyWatch_Logo_3.png')}
                        resizeMode={'cover'}
                    />
                    <Text style={styles.moneyWatchSubtitleText}>{subtitleTextLine1}</Text>
                    <Text style={styles.moneyWatchSubtitleText}>{subtitleTextLine2}</Text>
                </View>
        );
    }

    const NextButtonComponent = () => {
        return (
            <TouchableOpacity onPress={nextButtonAction}>
                <View style={styles.nextButtonView}>
                    <Text style={styles.nextText}>Next</Text>
                </View>
            </TouchableOpacity>
        );
    }

    const nextButtonAction = () => {
        navigation.navigate('AccountBalance');
    }

    return (
        <SafeAreaView style={styles.mainContentView}>
            <MoneyWatchInfoComponent />
            <NextButtonComponent />
        </SafeAreaView>
    );
}

export default WelcomeScreenView;