import React, {
    useState,
    useEffect,
    useRef,
    useContext
} from "react";
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Pressable,
    KeyboardAvoidingView,
    ScrollView,
    TextInput
} from "react-native";

import { SafeAreaView } from "react-native-safe-area-context";

import LottieView from 'lottie-react-native';

import SegmentedControl from "@react-native-segmented-control/segmented-control";

import TransactionType from "../../Constants/TransactionType";

import CalendarPicker from 'react-native-calendar-picker';
import { format, formatDistanceToNow } from "date-fns";

import { UserContext } from "../../Contexts/UserContext";

import { collection, doc, setDoc, addDoc, getDoc, getDocs, runTransaction, query, where, serverTimestamp } from "firebase/firestore";
import { firebaseAuthentication, firebaseCloudFirestoreDB } from "../../Firebase/FirebaseAuthentication";

const AddExpenseIncomeScreenView = ({ navigation, route }) => {

    const userData = useContext(UserContext);
    const [transactionType, setTransactionType] = useState(0);
    const [amount, setAmount] = useState('');
    const [date, setDate] = useState(null);
    const [formattedDate, setFormattedDate] = useState('');
    const [category, setCategory] = useState('');
    const [note, setNote] = useState('');
    const [shouldShowActivityIndicator, setShouldShowActivityIndicator] = useState(false);

    useEffect(() => {
        if (route.params?.category) {
            setCategory(route.params.category.title);
        }
        if (route.params?.selectedDate) {
            console.log('selectedDate: ', route.params.selectedDate);
            const selectedDate = route.params.selectedDate;
            setDate(selectedDate);
            const selectedDateObject = new Date(selectedDate);
            const selectedDateString = format(selectedDateObject, "dd-MM-yyyy, eee")
            setFormattedDate(selectedDateString);
        }
    }, [route.params?.category, route.params?.selectedDate]);

    const styles = StyleSheet.create({
        mainContentView: {
            flex: 1,
            backgroundColor: '#182C61',
            justifyContent: 'center',
            alignContent: 'center'
        },
        containerView: {
            backgroundColor: '#fff',
            marginHorizontal: 15,
            opacity: 0.8,
            borderStyle: 'solid',
            borderColor: 'black',
            borderWidth: 0.2,
            shadowColor: '#fff',
            shadowOpacity: 0.4,
            shadowRadius: 30,
            shadowOffset: { width: 13, height: 13 },
            borderRadius: 25
        },
        transactionTypeSegmentedControl: {
            alignSelf: 'center',
            width: '90%',
            marginTop: '10%'
        },
        textFieldsView: {
            alignItems: 'center',
            marginVertical: '8%'
        },
        inputFieldNameView: {
            marginLeft: '5%',
            width: '90%',
            marginTop: '2%',
            marginBottom: '1%',
            backgroundColor: 'transparent',
            justifyContent: 'flex-start'
        },
        inputFieldNameText: {
            color: '#3B3B98',
            fontSize: 14,
            fontWeight: '600',
        },
        inputField: {
            padding: 15,
            backgroundColor: '#3B3B98',
            borderColor: 'gray',
            borderRadius: 20,
            borderWidth: 0.5,
            width: '90%',
            marginVertical: '2%',
            color: '#F8EFBA',
            fontSize: 18,
            fontWeight: '700'
        },
        buttonsView: {
            //marginTop: 25,
            marginHorizontal: 30,
            //marginBottom: 35
            marginVertical: '8%'
        },
        buttonView: {
            alignSelf: 'center',
            width: '90%',
            backgroundColor: '#EAB543',
            padding: 20,
            marginBottom: 20,
            borderRadius: 45
        },
        buttonText: {
            fontSize: 18,
            fontWeight: '600',
            alignSelf: 'center',
            color: '#2C3A47'
        },
    });

    const AuthButtonComponent = () => {
        return (
            <View style={styles.buttonsView} >
                <TouchableOpacity onPress={authButtonAction}>
                    <View style={styles.buttonView}>
                        <Text style={styles.buttonText}>ADD</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }

    const TextInputFieldNameComponent = ({ name }) => {
        return (
            <View style={styles.inputFieldNameView}>
                <Text style={styles.inputFieldNameText}>{name}</Text>
            </View>
        );
    }

    const authButtonAction = () => {
        if ((amount == null || amount == '') || (date == null) || (category == null || category == '') || (note == null || note == '')) {
            alert('Please fill in all the fields to continue.');
        }
        else if (parseFloat(amount) == 0) {
            alert('Please enter an Amount greater than 0.');
        }
        else {
            addexpenseIncomeData();
        }
    }

    const addexpenseIncomeData = async () => {
        if (userData != null) {
            setShouldShowActivityIndicator(true);
            try {
                const accountsCollectionRef = collection(firebaseCloudFirestoreDB, "users", userData.userData.uid ,"accounts");
                const queryTemp = query(accountsCollectionRef, where("isSelected", "==", true));
                let selectedAccount = null;
                await runTransaction(firebaseCloudFirestoreDB, async (transaction) => {
                    const querySnapshot = await getDocs(queryTemp);
                    querySnapshot.forEach((doc) => {
                        console.log(doc.id, " => ", doc.data());
                        selectedAccount = doc.data();
                    });
                    console.log('selectedAccount: ', selectedAccount);

                });
                console.log("Transaction successfully committed!");

                const expenseIncomeCollectionName = 'expense-income-' + selectedAccount.name;
                const monthAndYearDate = new Date(date);
                const year = format(monthAndYearDate, "yyyy");
                const month = format(monthAndYearDate, "MMM");
                const day = format(monthAndYearDate, "ddd");
                const time = format(new Date(), "H:mm");
                console.log('expenseIncomeCollectionName: ', expenseIncomeCollectionName, 'year: ', year, 'month: ', month, 'day: ', day, 'time: ', time)
                const data = {
                    amount: parseFloat(amount),
                    date: formattedDate,
                    category: category,
                    note: note,
                    transactionType: (transactionType == 0) ? TransactionType.expense : TransactionType.income,
                    serverTimestamp: serverTimestamp()
                };
                const ref = doc(firebaseCloudFirestoreDB, "users", userData.userData.uid , expenseIncomeCollectionName, year, month + year, day + time + month + year);
                const docRef = setDoc(ref, data, { merge: true });
                console.log("Document written with ID: ", docRef);
                setShouldShowActivityIndicator(false);

                setAmount('');
                setFormattedDate('');
                setCategory('');
                setNote('');


                let transactionTypeString = '';
                if (transactionType == 0) {
                    transactionTypeString = TransactionType.expense;
                }
                else if (transactionType == 1) {
                    transactionTypeString = TransactionType.income;
                }

                navigation.navigate('Home', {
                    isExpenseIncomeEntryAdded: true
                });
                alert(transactionTypeString + '  entry added successfully!');
            }
            catch (error) {
                setShouldShowActivityIndicator(false);
                alert(error.message);
            }
        }
        else {
            alert('There was some error in accessing your stored data. Please sign in again to authenticate.');
        }
    }

    const dateFieldAction = () => {
        navigation.navigate('CalendarPicker');
    }

    const categoryFieldAction = () => {
        navigation.navigate('Categories', { categorySelectionMode: true });
    }

    return (
        <SafeAreaView style={styles.mainContentView}>
            <KeyboardAvoidingView>
                <View style={styles.containerView}>
                    <SegmentedControl
                        style={styles.transactionTypeSegmentedControl}
                        values={[TransactionType.expense, TransactionType.income]}
                        selectedIndex={transactionType}
                        onChange={(event) => {
                            setTransactionType(event.nativeEvent.selectedSegmentIndex);
                        }}
                        backgroundColor={'#3B3B98'}
                        tintColor={'#EAB543'}
                    />
                    <View style={styles.textFieldsView} >
                        <TextInputFieldNameComponent name="Amount" />
                        <TextInput placeholder={'Set Amount'} placeholderTextColor={'#fff'} value={amount} onChangeText={(text) => setAmount(text)} keyboardType={'numeric'} returnKeyType={'done'} style={styles.inputField} />
                        <TextInputFieldNameComponent name="Date" />
                        <TextInput placeholder={'Select Date'} placeholderTextColor={'#fff'} value={formattedDate} autoCapitalize={'sentences'} onTouchStart={dateFieldAction} editable={false} style={styles.inputField} />
                        <TextInputFieldNameComponent name="Category" />
                        <TextInput placeholder={'Select a Category'} placeholderTextColor={'#fff'} value={category} onChangeText={(text) => setCategory(text)} autoCapitalize={'sentences'} onTouchStart={categoryFieldAction} editable={false} style={styles.inputField} />
                        <TextInputFieldNameComponent name="Note" />
                        <TextInput placeholder={'Add Note'} value={note} onChangeText={(text) => setNote(text)} placeholderTextColor={'#fff'} style={styles.inputField} />
                    </View>
                    <AuthButtonComponent />
                </View>
            </KeyboardAvoidingView>
            {(shouldShowActivityIndicator) ? <LottieView source={require('../../Assets/Loading-Spinner-Animation.json')} autoPlay loop colorFilters={[
                {
                    keypath: 'Line',
                    color: '#EAB543',
                },
                {
                    keypath: 'Line transparent',
                    color: '#111',
                },
            ]} /> : null}
        </SafeAreaView>
    );
}

export default AddExpenseIncomeScreenView;