import React, {
    useState,
    useLayoutEffect
} from "react";
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity
} from "react-native";

import { SafeAreaView } from "react-native-safe-area-context";

import CalendarPicker from 'react-native-calendar-picker';

import FontistoIcon from 'react-native-vector-icons/Fontisto';
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";

const CalendarPickerScreenView = ({ navigation }) => {

let date = null;

    useLayoutEffect(() => {
        navigation.setOptions({
            headerRight: () => (
                <TouchableOpacity onPress={closeButtonAction}>
                    <FontAwesome5Icon name={"times-circle"} size={25} color={"#3B3B98"} />
                </TouchableOpacity>
            ),
        });
    }, [navigation]);

    const closeButtonAction = () => {
        if (date != null) {
            navigation.navigate('AddExpenseIncome', { 
                selectedDate: date 
             });
        }
        else {
            navigation.goBack();
        }
    }

    const styles = StyleSheet.create({
        mainContentView: {
            flex: 1,
            backgroundColor: 'transparent',
            justifyContent: 'center',
            alignContent: 'center'
        }
    });

    function onDateChange(dateValue, dateType) {
        date = dateValue
        closeButtonAction();
    }

    return (
        <SafeAreaView style={styles.mainContentView}>
            <CalendarPicker
                onDateChange={onDateChange}
            />
        </SafeAreaView>
    );
}

export default CalendarPickerScreenView;