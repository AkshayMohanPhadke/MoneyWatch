import React from "react";
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity
} from "react-native";

import { SafeAreaView } from "react-native-safe-area-context";

import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';

import AuthSelectionType from "../../Constants/AuthSelectionType";

const AuthSelectionScreenView = ({ navigation }) => {

    const styles = StyleSheet.create({
        mainContentView: {
            flex: 1,
            backgroundColor: '#182C61',
            justifyContent: 'center'
        },
        moneyWatchView: {
            // marginTop: '8%',
             marginBottom: '15%'
        },
        moneyWatchText: {
            fontSize: 40,
            fontWeight: 'bold',
            alignSelf: 'center',
            color: '#fff',
            marginBottom: '10%'
        },
        signUpInfoText: {
            fontSize: 17.5,
            fontWeight: '600',
            color: '#EAB543',
            alignSelf: 'center'
        },
        loginSignUpButtonsComponent: {
            marginHorizontal: 50,
            backgroundColor: 'transparent',
            marginVertical: '15%'
        },
        signUpButtonView: {
            alignSelf: 'center',
            width: '90%',
            backgroundColor: '#EAB543',
            padding: 20,
            marginBottom: '15%',
            borderRadius: 45
        },
        signUpText: {
            fontSize: 18,
            fontWeight: '600',
            alignSelf: 'center',
            color: '#2C3A47'
        },
        loginText: {
            fontSize: 20,
            fontWeight: '600',
            color: '#EAB543',
            alignSelf: 'center'
        },
        socialLoginButtonsComponent: {
            marginHorizontal: 50,
            backgroundColor: 'transparent',
            marginTop: '5%',
        },
        loginWithText: {
            fontSize: 17.5,
            fontWeight: '600',
            color: '#F8EFBA',
            alignSelf: 'center',
            marginVertical: '10%'
        },
        socialLoginButtonIconsView: {
            marginHorizontal: 50,
            flexDirection: 'row',
            //alignItems: 'flex-end'
            justifyContent: 'space-around',
            marginVertical: '5%%'
        },
        skipTextView: {
            marginHorizontal: 50,
            fontSize: 17.5,
            fontWeight: '600',
            color: '#F8EFBA',
            marginTop: '15%',
            height: 50
        },
        skipText: {
            fontSize: 17.5,
            fontWeight: '600',
            color: '#EAB543',
            alignSelf: 'center'
        }
    });


    const MoneyWatchInfoComponent = () => {
        return (
            <View style={styles.moneyWatchView}>
                <Text style={styles.moneyWatchText}>Money Watch</Text>
            </View>
        );
    }

    const SignUpInfoTextComponent = () => {
        return (
            <Text style={styles.signUpInfoText}>
                Sign Up to save your data
            </Text>
        );
    }

    const LoginSignUpButtonsComponent = () => {
        return (
            <View style={styles.loginSignUpButtonsComponent}>
                <TouchableOpacity onPress={signUpButtonAction}>
                    <View style={styles.signUpButtonView}>
                        <Text style={styles.signUpText}>Sign Up</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={loginButtonAction}>
                    <Text style={styles.loginText}>Login</Text>
                </TouchableOpacity>
            </View>
        );
    }

    const SocialLoginButtonsComponent = () => {
        return (
            <View style={styles.socialLoginButtonsComponent}>
                <Text style={styles.loginWithText}>Login with:</Text>
                <View style={styles.socialLoginButtonIconsView}>
                    <TouchableOpacity>
                        <FontAwesome5Icon name={'facebook-square'} size={40} color={'#F8EFBA'} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <FontAwesome5Icon name={'google'} size={40} color={'#F8EFBA'} />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    const SkipTextComponent = () => {
        return (
            <TouchableOpacity onPress={skipButtonAction}>
                <View style={styles.skipTextView}>
                    <Text style={styles.skipText}>
                        Continue without signing up
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }

    const loginButtonAction = () => {
        navigation.navigate('LoginSignUp', { authSelectionType: AuthSelectionType.login });
    }

    const signUpButtonAction = () => {
        navigation.navigate('LoginSignUp', { authSelectionType: AuthSelectionType.signUp });
    }

    const skipButtonAction = () => {
        navigation.navigate('Welcome');
    }

    return (
        <SafeAreaView style={styles.mainContentView}>
            <MoneyWatchInfoComponent />
            <SignUpInfoTextComponent />
            <LoginSignUpButtonsComponent />
            <SocialLoginButtonsComponent />
            {/* <SkipTextComponent /> */}
        </SafeAreaView>
    );
}

export default AuthSelectionScreenView;