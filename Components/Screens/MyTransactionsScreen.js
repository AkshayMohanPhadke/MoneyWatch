import React from "react";
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity
} from "react-native";

import { SafeAreaView } from "react-native-safe-area-context";

const MyTransactionsScreenView = () => {

    const styles = StyleSheet.create({
        mainContentView: {
            flex: 1,
            backgroundColor: '#182C61'
        },
        signUpInfoText: {
            marginTop: '10%',
            fontSize: 17.5,
            fontWeight: '600',
            color: '#EAB543',
            alignSelf: 'center'
        },
        loginSignUpButtonsComponent: {
            marginHorizontal: 50,
            backgroundColor: 'transparent',
            marginVertical: '15%'
        },
        signUpButtonView: {
            alignSelf: 'center',
            width: '90%',
            backgroundColor: '#EAB543',
            padding: 20,
            marginBottom: 30,
            borderRadius: 45
        },
        signUpText: {
            fontSize: 16,
            fontWeight: '600',
            alignSelf: 'center',
            color: '#2C3A47'
        },
        loginText: {
            fontSize: 17.5,
            fontWeight: '600',
            color: '#EAB543',
            alignSelf: 'center'
        },
        socialLoginButtonsComponent: {
            marginHorizontal: 50,
            backgroundColor: 'transparent',
            marginTop: '5%',
        },
        loginWithText: {
            fontSize: 17.5,
            fontWeight: '600',
            color: '#F8EFBA',
            alignSelf: 'center',
            marginVertical: '10%'
        },
        socialLoginButtonIconsView: {
            marginHorizontal: 50,
            flexDirection: 'row',
            //alignItems: 'flex-end'
            justifyContent: 'space-around',
            marginVertical: '5%%'
        },
        skipTextView: {
            marginHorizontal: 50,
            fontSize: 17.5,
            fontWeight: '600',
            color: '#F8EFBA',
            marginTop: '15%',
            height: 50
        },
        skipText: {
            fontSize: 17.5,
            fontWeight: '600',
            color: '#F8EFBA',
            alignSelf: 'center'
        }
    });

    return (
        <SafeAreaView style={styles.mainContentView}>
        </SafeAreaView>
    );
}

export default MyTransactionsScreenView;