import React, {
    useState
} from "react";
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    KeyboardAvoidingView,
    TextInput
} from "react-native";

import { SafeAreaView } from "react-native-safe-area-context";

import LottieView from 'lottie-react-native';

import Constants from "../../Constants/Constants";
import AuthSelectionType from "../../Constants/AuthSelectionType";

import { createUserWithEmailAndPassword, signInWithEmailAndPassword } from "firebase/auth";
import { collection, doc, setDoc, addDoc, getDoc, getDocs } from "firebase/firestore";
import { firebaseAuthentication, firebaseCloudFirestoreDB } from "../../Firebase/FirebaseAuthentication";

const LoginSignUpScreenView = ({ navigation, route }) => {

    const passwordLength = 8;
    const emailInvalidErrorMessage = 'Please enter a valid email address';
    const emailBlankErrorMessage = 'Email is blank';
    const passwordInvalidErrorMessage = 'Password should contain at least 8 characters';
    const passwordBlankErrorMessage = 'Password is blank';

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [user, setUser] = useState(null);
    const [shouldShowActivityIndicator, setShouldShowActivityIndicator] = useState(false);
    const [isEmailValid, setIsEmailValid] = useState(false);
    const [isPasswordValid, setIsPasswordValid] = useState(false);
    const [emailErrorMessage, setEmailErrorMessage] = useState('');
    const [passwordErrorMessage, setPasswordErrorMessage] = useState('');

    const { authSelectionType } = route.params;

    const styles = StyleSheet.create({
        mainContentView: {
            flex: 1,
            backgroundColor: '#182C61',
            justifyContent: 'center',
            alignContent: 'center'
        },
        containerView: {
            //flex: 0,
            //justifyContent: 'flex-end',
            backgroundColor: '#fff',
            marginHorizontal: 15,
            //alignContent: 'center'
            //marginVertical: '5%',
            opacity: 0.8,
            borderStyle: 'solid',
            borderColor: 'black',
            borderWidth: 0.2,
            shadowColor: '#fff',
            shadowOpacity: 0.4,
            shadowRadius: 30,
            shadowOffset: { width: 13, height: 13 },
            borderRadius: 25
        },
        textFieldsView: {
            //marginHorizontal: 15,
            //alignContent: 'center',
            alignItems: 'center',
            //padding: 20,
            marginVertical: '8%'
        },
        emailTextField: {
            padding: 15,
            backgroundColor: '#3B3B98',
            borderColor: 'gray',
            borderRadius: 20,
            borderWidth: 0.5,
            width: '90%',
            marginVertical: '2%',
            color: '#F8EFBA',
            fontSize: 18,
            fontWeight: '700'
        },
        passwordTextField: {
            padding: 15,
            backgroundColor: '#3B3B98',
            borderColor: 'gray',
            borderRadius: 20,
            borderWidth: 0.5,
            width: '90%',
            color: '#F8EFBA',
            fontSize: 18,
            fontWeight: '700',
            marginTop: '2%'
        },
        buttonsView: {
            //marginTop: 25,
            marginHorizontal: 30,
            //marginBottom: 35
            marginVertical: '8%'
        },
        loginButtonView: {
            alignSelf: 'center',
            width: '90%',
            backgroundColor: '#EAB543',
            padding: 20,
            marginBottom: 20,
            borderRadius: 45
        },
        loginText: {
            fontSize: 18,
            fontWeight: '600',
            alignSelf: 'center',
            color: '#2C3A47'
        },
        isEmailValidTextView: {
            width: '90%',
            marginLeft: 20,
            marginBottom: '3%'

        },
        isEmailValidText: {
            fontSize: 15,
            fontWeight: '600',
            textAlign: 'left',
            color: 'red'
        },
        isPasswordValidTextView: {
            width: '90%',
            marginLeft: 20,
            alignContent: 'flex-start',
            marginVertical: '2%'
        },
        isPasswordValidText: {
            fontSize: 15,
            fontWeight: '600',
            textAlign: 'left',
            color: 'red'
        },
        lottieView: {
            width: 200,
            height: 200
        }
    });

    const AuthButtonComponent = () => {
        return (
            <View style={styles.buttonsView} >
                <TouchableOpacity onPress={authButtonAction}>
                    <View style={styles.loginButtonView}>
                        <Text style={styles.loginText}>{(authSelectionType == AuthSelectionType.login) ? 'Login' : 'Sign Up'}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }

    const addUser = async (user) => {
        try {
            const docRef = doc(firebaseCloudFirestoreDB, "users", user.uid);
            const setDocRef = setDoc(docRef, user, { merge: true });
            console.log("setDoc Document written: ", setDocRef);
            addCategories(user);
        }
        catch (error) {
            setShouldShowActivityIndicator(false);
            alert(error.message);
        }
    }

    const addCategories = async (user) => {
        try {
            const uid = user.uid;
            Constants.categories.forEach(category => {

                let categoryObject = {
                    title: category.title,
                    image: category.image,
                    color: generateRandomColor()
                }

                const docRef = doc(firebaseCloudFirestoreDB, "users",uid,"categories", categoryObject.title);
                const setDocRef = setDoc(docRef, categoryObject, { merge: true });
                console.log("setDoc Document written: ", setDocRef);
            });
            setShouldShowActivityIndicator(false);
        }
        catch (error) {
            setShouldShowActivityIndicator(false);
            alert(error.message);
        }
    }

    const generateRandomColor = () => {
        //return '#' + Math.floor(Math.random()*16777215).toString(16).slice(2, 8).toUpperCase().slice(-6);
        //return '#' +  Math.random().toString(16).substring(-6);
        return '#' + ("000000" + Math.random().toString(16).slice(2, 8).toUpperCase()).slice(-6);
      }

    const authButtonAction = () => {
        if ((email == null || email == '') && (password == null || password == '')) {
            setEmailErrorMessage(emailBlankErrorMessage);
            setPasswordErrorMessage(passwordBlankErrorMessage);
            setIsEmailValid(true);
            setIsPasswordValid(true);
            console.log(emailErrorMessage); console.log(passwordErrorMessage);
        }
        else if ((isEmailValid && isPasswordValid) || (isEmailValid || isPasswordValid)) {
            if (email == null || email == '') {
                setEmailErrorMessage(emailBlankErrorMessage);
                setIsEmailValid(true);
            }
            else if (password == null || password == '') {
                setPasswordErrorMessage(passwordBlankErrorMessage);
                setIsPasswordValid(true);
            }
        }
        else {

            if ((email == null || email == '') && (password == null || password == '')) {
                setEmailErrorMessage(emailBlankErrorMessage);
                setPasswordErrorMessage(passwordBlankErrorMessage);
                setIsEmailValid(true);
                setIsPasswordValid(true);
                console.log(emailErrorMessage); console.log(passwordErrorMessage);
            }
            else if ((email == null || email == '') || (password == null || password == '')) {
                if (email == null || email == '') {
                    setEmailErrorMessage(emailBlankErrorMessage);
                    setIsEmailValid(true);
                }
                else if (password == null || password == '') {
                    setPasswordErrorMessage(passwordBlankErrorMessage);
                    setIsPasswordValid(true);
                }
                console.log(emailErrorMessage); console.log(passwordErrorMessage);
            }
            else {
                setShouldShowActivityIndicator(true);
                switch (authSelectionType) {
                    case AuthSelectionType.login:
                        signInWithEmailAndPassword(firebaseAuthentication, email, password)
                            .then(userCredentials => {
                                const user = userCredentials.user;
                                console.log(user);
                                const userData = {
                                    uid: user.uid,
                                    displayName: user.displayName,
                                    email: user.email,
                                    phoneNumber: user.phoneNumber,
                                    photoURL: user.photoURL,
                                    refreshToken: user.refreshToken
                                }
                                addUser(userData);
                            })
                            .catch(error => {
                                setShouldShowActivityIndicator(false);
                                alert(error.message)
                            })
                        break;
                    case AuthSelectionType.signUp:
                        createUserWithEmailAndPassword(firebaseAuthentication, email, password)
                            .then(userCredentials => {
                                const user = userCredentials.user;
                                console.log(user);
                                setShouldShowActivityIndicator(false);
                                navigation.navigate('Welcome');
                            })
                            .catch(error => {
                                setShouldShowActivityIndicator(false);
                                alert(error)
                            })
                        break;
                }
            }
        }
    }

    const validateEmail = (text) => {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
        if (reg.test(text) === false) {
            console.log("Email is Not Correct");
            setEmail(text);
            if (text.length == 0) {
                setIsEmailValid(false);
                setEmailErrorMessage('');
            }
            else {
                setIsEmailValid(true);
                setEmailErrorMessage(emailInvalidErrorMessage);
            }

            return false;
        }
        else {
            setEmail(text);
            setIsEmailValid(false);
            console.log("Email is Correct");
        }
    }

    const validatePassword = (text) => {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
        if (text.length < passwordLength) {
            console.log("Password is Not Correct");
            setPassword(text);
            if (text.length == 0) {
                setIsPasswordValid(false);
                setPasswordErrorMessage('');
            }
            else {
                setIsPasswordValid(true);
                setPasswordErrorMessage(passwordInvalidErrorMessage);
            }
            return false;
        }
        else if (text.length >= passwordLength) {
            setPassword(text);
            setIsPasswordValid(false);
            console.log("Password is Correct");
        }
    }

    return (
        <SafeAreaView style={styles.mainContentView}>
            <KeyboardAvoidingView>
                <View style={styles.containerView}>
                    <View style={styles.textFieldsView} >
                        <TextInput placeholder={'Email'} placeholderTextColor={'#fff'} value={email} autoCapitalize={'none'} onChangeText={(text) => validateEmail(text)} style={styles.emailTextField} />
                        {(isEmailValid) ?
                            <View style={styles.isEmailValidTextView}>
                                <Text style={styles.isEmailValidText}>{emailErrorMessage}</Text>
                            </View> : null
                        }
                        <TextInput placeholder={'Password'} placeholderTextColor={'#fff'} value={password} onChangeText={(text) => validatePassword(text)} secureTextEntry={true} style={styles.passwordTextField} />
                        {(isPasswordValid) ?
                            <View style={styles.isPasswordValidTextView}>
                                <Text style={styles.isPasswordValidText}>{passwordErrorMessage}</Text>
                            </View> : null
                        }
                    </View>
                    <AuthButtonComponent />
                </View>
            </KeyboardAvoidingView>
            {(shouldShowActivityIndicator) ? <LottieView source={require('../../Assets/Loading-Spinner-Animation.json')} autoPlay loop colorFilters={[
                {
                    keypath: 'Line',
                    color: '#EAB543',
                },
                {
                    keypath: 'Line transparent',
                    color: '#111',
                },
            ]} /> : null}
        </SafeAreaView>
    );
}

export default LoginSignUpScreenView;