import { NavigationContainer } from "@react-navigation/native";
import React, {
    useState,
    useEffect,
    useRef
} from "react";
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    SectionList
} from "react-native";

import { SafeAreaView } from "react-native-safe-area-context";

import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcon from "react-native-vector-icons/MaterialCommunityIcons";

import DropDownPicker from 'react-native-dropdown-picker';

import SegmentedControl from "@react-native-segmented-control/segmented-control";

import TransactionType from "../../Constants/TransactionType";
import TransactionViewType from "../../Constants/TransactionViewType";
import TransactionViewTypeIndex from "../../Constants/TransactionViewTypeIndex";
import DateViewTypeButtonAction from "../../Constants/DateViewTypeButtonAction";
import StateValue from "../../Constants/State";

import moment from "moment";
import { format, setYear, sub, parse, parseISO, min, getISODay, getISOWeek, getDay, isEqual, isAfter, isBefore, getDaysInMonth } from "date-fns";

import CalendarPickerScreenView from "./CalendarPickerScreen";
import { Date } from "core-js";

import DateViewDataModel from "../../ModelsAndInterfaces/DateViewDataModel";
import Constants from "../../Constants/Constants";

const HomeScreenView = ({ navigation, route }) => {

    const [viewTypeIndex, setViewTypeIndex] = useState(0);
    const [dateViewString, setDateViewString] = useState('');
    const [isLeftButtonDisabled, setIsLeftButtonDisabled] = useState(false);
    const [isRightButtonDisabled, setIsRightButtonDisabled] = useState(false);
    let minDate = useRef('');
    let maxDate = useRef('');
    let day = useRef('');
    let month = useRef('');
    let daysInCurrentMonth = useRef(0);
    let daysInPreviousMonth = useRef(0);
    let daysInNextMonth = useRef(0);
    let monthNumber = useRef(0);
    let week = useRef('');
    let weekday = useRef('');
    let year = useRef('');

    const data = [
        {
            title: '12 December 2021, Sunday',
            data: [{
                amount: '650',
                transactionType: TransactionType.expense,
                date: '12-12-2021',
                category: 'Groceries',
                note: 'Big Basket'
            },
            {
                amount: '440.50',
                transactionType: TransactionType.expense,
                date: '12-12-2021',
                category: 'Lunch',
                note: 'Punjab Canteen'
            },
            {
                amount: '100',
                transactionType: TransactionType.expense,
                date: '12-12-2021',
                category: 'Other',
                note: 'Stationery'
            }]
        },
        {
            title: '10 December 2021, Friday',
            data: [{
                amount: '290',
                transactionType: TransactionType.expense,
                date: '10-12-2021',
                category: 'Sweets',
                note: "Haldiram's Kaju Roll"
            },
            {
                amount: '450',
                transactionType: TransactionType.expense,
                date: '10-12-2021',
                category: 'Mobile Postpaid Bill',
                note: 'Vi Postpaid Bill Nov 2021'
            }]
        },
        {
            title: '07 December 2021, Tuesday',
            data: [{
                amount: '65000.45',
                transactionType: TransactionType.income,
                date: '07-12-2021',
                category: 'Interest',
                note: 'Quarterly Savings Acc Interest'
            },
            {
                amount: '550',
                transactionType: TransactionType.expense,
                date: '07-12-2021',
                category: 'Groceries',
                note: 'Flipkart Quick'
            },
            {
                amount: '750',
                transactionType: TransactionType.expense,
                date: '07-12-2021',
                category: 'Winter Wear',
                note: 'Sweat-Jacket from Amazon'
            }]
        },
        {
            title: '03 December 2021, Friday',
            data: [{
                amount: '1000',
                transactionType: TransactionType.income,
                date: '03-12-2021',
                category: 'Reimbursement',
                note: 'Team Lunch Reimbursement'
            }]
        },
        {
            title: '01 December 2021, Wednesday',
            data: [{
                amount: '200',
                transactionType: TransactionType.expense,
                date: '01-12-2021',
                category: 'Gifts',
                note: 'Birthday Gift for Cousin'
            }]
        }
    ];

    const styles = StyleSheet.create({
        mainContentView: {
            flex: 1,
            backgroundColor: '#182C61',
        },
        dropDownContainerView: {
            flexDirection: 'row',
            //justifyContent: 'space-between',
            //paddingHorizontal: 20,
            marginHorizontal: 10,

        },
        monthDropDownContainerView: {
            width: '50%',
        },
        yearDropDownContainerView: {
            width: '50%',
        },
        menuIconTouchableOpacity: {
            left: '5%',
            top: '7%',
            position: 'absolute',
            alignItems: 'center',
            justifyContent: 'center',
            width: 33,
            height: 33
        },
        viewTypeSelectionComponent: {
            marginTop: '15%',
            marginHorizontal: '2%',
            flexDirection: 'row',
            justifyContent: 'space-between'
        },
        leftChevronEnabledTouchableOpacity: {
            alignItems: 'center',
            justifyContent: 'center',
            width: 40,
            height: 40,
            opacity: 1
        },
        leftChevronDisabledTouchableOpacity: {
            alignItems: 'center',
            justifyContent: 'center',
            width: 40,
            height: 40,
            opacity: 0.3
        },
        rightChevronEnabledTouchableOpacity: {
            alignItems: 'center',
            justifyContent: 'center',
            width: 40,
            height: 40,
            opacity: 1
        },
        rightChevronDisabledTouchableOpacity: {
            alignItems: 'center',
            justifyContent: 'center',
            width: 40,
            height: 40,
            opacity: 0.3
        },
        viewTypeDateOpacity: {
            alignItems: 'center',
            justifyContent: 'center',

        },
        viewTypeDateText: {
            fontWeight: '600',
            fontSize: 19,
            color: '#EAB543'
        },
        viewTypeSegmentedControl: {
            alignSelf: 'center',
            marginVertical: '5%',
            marginHorizontal: '5%',
            width: '95%'
        },
        addExpenseIncomeTouchableOpacity: {
            right: '10%',
            bottom: '5%',
            position: 'absolute',
            alignItems: 'center',
            justifyContent: 'center',
            width: 50,
            height: 50
        },
        addExpenseIncomeIcon: {

        },
        list: {

        },
        sectionItemView: {
            padding: 20,
            marginHorizontal: '6%',
            backgroundColor: 'white',
            marginVertical: 0,
            // borderRadius: 5,
            borderColor: 'gray',
            borderWidth: 0.3,
            flexDirection: 'row',
            justifyContent: 'space-between',
            shadowRadius: 9,
            shadowColor: 'gray',
            shadowOpacity: 0.7,
            shadowOffset: { width: 4.5, height: 4.5 }
        },
        titleView: {
            width: '50%'
        },
        priceView: {
            width: '50%'
        },
        amountView: {
            justifyContent: 'center',
        },
        title: {
            fontWeight: '600',
            fontSize: 17,
            marginBottom: '4%'
        },
        category: {
            fontWeight: '300',
            fontSize: 15
        },
        expenseAmount: {
            fontWeight: 'bold',
            fontSize: 17,
            color: 'red'
        },
        incomeAmount: {
            fontWeight: '700',
            fontSize: 17,
            color: 'green'
        },
        sectionHeaderView: {
            padding: 20,
            justifyContent: 'center',
            backgroundColor: '#fff',
            marginHorizontal: '6%',
            marginTop: '6%',
            borderTopLeftRadius: 9,
            borderTopRightRadius: 9,
            borderColor: 'gray',
            borderWidth: 0.3,
            shadowRadius: 9,
            shadowColor: 'gray',
            shadowOpacity: 0.7,
            shadowOffset: { width: 4.5, height: 4.5 }
        },
        sectionHeaderTitle: {
            fontSize: 16,
            fontWeight: '700',
            color: '#3B3B98',
            textAlign: 'left'
        }
    });

    useEffect(() => {
        if (route.params?.isExpenseIncomeEntryAdded) {

        }
        else {
            initialDateSetup();
        }
    }, [route.params?.isExpenseIncomeEntryAdded]);

    //MARK:- Helper Methods
    function initialDateSetup() {
        const minDateObject = parse(Constants.minDateString, 'dd/MM/yyyy', new Date());
        minDate.current = minDateObject.toString();
        console.log('min date: ', minDateObject);
        const maxDateObject = new Date();
        maxDate.current = maxDateObject.toString();

        day.current = format(new Date(maxDate.current), 'dd');
        month.current = format(new Date(maxDate.current), 'MMM');
        monthNumber.current = format(new Date(maxDate.current), 'MM');
        year.current = format(new Date(maxDate.current), 'yyyy');

        daysInPreviousMonth.current = daysInMonth(StateValue.previous);
        daysInCurrentMonth.current = daysInMonth(StateValue.current);
        daysInNextMonth.current = daysInMonth(StateValue.next);

        setDateView(viewTypeIndex, DateViewTypeButtonAction.none);
    }

    function computeDayValue(dateViewTypeButtonAction) {
        if (dateViewTypeButtonAction != DateViewTypeButtonAction.none) {
            let dayIntValue = parseInt(day.current);
            daysInPreviousMonth.current = daysInMonth(StateValue.previous);
            daysInCurrentMonth.current = daysInMonth(StateValue.current);
            daysInNextMonth.current = daysInMonth(StateValue.next);
            switch (dateViewTypeButtonAction) {
                case DateViewTypeButtonAction.left:
                    dayIntValue = dayIntValue - 1;
                    if (dayIntValue < 1) {
                        dayIntValue = daysInPreviousMonth.current;
                        computeMonthValue(StateValue.previous);
                        computeYearValue(StateValue.previous);
                    }
                    break
                case DateViewTypeButtonAction.right:
                    dayIntValue = dayIntValue + 1;
                    if (dayIntValue > daysInCurrentMonth.current) {
                        dayIntValue = 1;
                        computeMonthValue(StateValue.next);
                        computeYearValue(StateValue.next);
                    }
                    break
            }
            day.current = dayIntValue.toString();
            computeMonthValue();
        }
        day.current = format(new Date(day.current + '-' + month.current + '-' + year.current), 'dd');
        console.log('computed day: ', day.current, ' month: ', month.current, ' year: ', year.current);
    }

    function computeWeekValue(dateViewTypeButtonAction) {
        let isoDay = getISODay(parse(day.current + '/' + month.current + '/' + year.current, 'dd/MMM/yyyy', new Date()));
        let firstDayOfWeek, lastDayOfWeek;
        if (isoDay == 1) {
            firstDayOfWeek = parseInt(day.current);
            lastDayOfWeek = parseInt(day.current) + 7;
        }
        else if (isoDay == 7) {
            firstDayOfWeek = parseInt(day.current) - 7;
            lastDayOfWeek = parseInt(day.current);
        }
        else {
            const difference = 7 - isoDay;
            firstDayOfWeek = (parseInt(day.current) - isoDay) + 1;
            lastDayOfWeek = parseInt(day.current) + difference;
        }
        week.current = firstDayOfWeek + '  -  ' + lastDayOfWeek + '   ' + month.current;
    }

    function computeMonthValue(stateValue) {
        if (stateValue != undefined) {
            let monthNumberValue = parseInt(monthNumber.current);
            let yearValue = parseInt(year.current);
            switch (stateValue) {
                case StateValue.previous:
                    monthNumberValue = monthNumberValue - 1;
                    if (monthNumberValue < 1) {
                        monthNumberValue = 12;
                        yearValue = yearValue - 1;
                    }
                    break;
                case StateValue.next:
                    monthNumberValue = monthNumberValue + 1;
                    if (monthNumberValue > 12) {
                        monthNumberValue = 1;
                        yearValue = yearValue + 1;
                    }
                    break;
            }
            monthNumber.current = monthNumberValue;
            month.current = format(new Date(year.current + '/' + monthNumber.current + '/' + day.current), 'MMM');
            year.current = yearValue.toString();
        }
        else {
            month.current = format(new Date(year.current + '/' + monthNumber.current + '/' + day.current), 'MMM');
            monthNumber.current = format(new Date(day.current + '-' + month.current + '-' + year.current), 'MM');
        }

        console.log('computed day: ', day.current, ' month: ', month.current, ' year: ', year.current);
    }

    function computeYearValue(stateValue) {
        if (stateValue != undefined) {
            let yearValue = parseInt(year.current);
            if (stateValue == StateValue.previous) {
                yearValue = yearValue - 1;
            }
            else if (stateValue == StateValue.next) {
                yearValue = yearValue + 1;
            }
            year.current = yearValue;
        }
        else {
            year.current = format(new Date(day.current + '-' + month.current + '-' + year.current), 'yyyy');
        }

        console.log('computed day: ', day.current, ' month: ', month.current, ' year: ', year.current);
    }

    function daysInMonth(stateValue) {
        let daysInMonth = 0;
        switch (stateValue) {
            case StateValue.previous:
                daysInMonth = getDaysInMonth(new Date(day.current + '-' + getMonthName(stateValue) + '-' + year.current));
                break;
            case StateValue.current:
                daysInMonth = getDaysInMonth(new Date(day.current + '-' + month.current + '-' + year.current));
                break;
            case StateValue.next:
                daysInMonth = getDaysInMonth(new Date(day.current + '-' + getMonthName(stateValue) + '-' + year.current));
                break;
        }
        return daysInMonth;
    }

    function getMonthName(stateValue) {
        let monthNumberValue = parseInt(monthNumber.current);
        switch (stateValue) {
            case StateValue.previous:
                monthNumberValue = monthNumberValue - 1;
                if (monthNumberValue < 1) {
                    monthNumberValue = 12;
                }
                break;
            case StateValue.next:
                monthNumberValue = monthNumberValue + 1;
                if (monthNumberValue > 12) {
                    monthNumberValue = 1;
                }
                break;
        }
        return format(new Date(monthNumberValue.toString() + '/' + day.current + '/' + year.current), 'MMM');
    }

    function setDateView(selectedIndex, dateViewTypeButtonAction) {
        let stateValue;
        switch (selectedIndex) {
            case TransactionViewTypeIndex.day:
                computeDayValue(dateViewTypeButtonAction);
                setDateViewString(day.current);
                break;
            case TransactionViewTypeIndex.week:
                computeWeekValue(dateViewTypeButtonAction);
                setDateViewString(week.current);
                break;
            case TransactionViewTypeIndex.month:
                switch (dateViewTypeButtonAction) {
                    case DateViewTypeButtonAction.left:
                        stateValue = StateValue.previous;
                        break;
                    case DateViewTypeButtonAction.right:
                        stateValue = StateValue.next;
                        break;
                }
                computeMonthValue(stateValue);
                setDateViewString(month.current);
                break;
            case TransactionViewTypeIndex.year:
                switch (dateViewTypeButtonAction) {
                    case DateViewTypeButtonAction.left:
                        stateValue = StateValue.previous;
                        break;
                    case DateViewTypeButtonAction.right:
                        stateValue = StateValue.next;
                        break;
                }
                computeYearValue(stateValue);
                setDateViewString(year.current);
                break;
        }
        updateLeftAndRightButtonsStatus();
    }

    function updateLeftAndRightButtonsStatus() {
        
        const currentDateObject = new Date(day.current + '-' + month.current + '-' + year.current);
        const currentDateDayInt = parseInt(format(new Date(currentDateObject), 'dd'))
        const currentDateMonthString = format(new Date(currentDateObject), 'MMM');
        const currentDateMonthNumber = parseInt(format(new Date(currentDateObject), 'MM'));
        const currentDateYearInt = parseInt(format(new Date(currentDateObject), 'yyyy'));

        const maxDateDayString = format(new Date(maxDate.current), 'dd');
        const maxDateMonthString = format(new Date(maxDate.current), 'MMM');
        const maxDateYearString = format(new Date(maxDate.current), 'yyyy');
        const maxDateDayInt = format(new Date(maxDate.current), 'dd');
        const maxDateMonthNumber = parseInt(format(new Date(maxDate.current), 'MM'));
        const maxDateYearInt = format(new Date(maxDate.current), 'yyyy');
        const maxDateObject = new Date(maxDateDayString + '-' + maxDateMonthString + '-' + maxDateYearString);
        
        const minDateDayString = format(new Date(minDate.current), 'dd');
        const minDateMonthString = format(new Date(minDate.current), 'MMM');
        const minDateYearString = format(new Date(minDate.current), 'yyyy');
        const minDateDayInt = format(new Date(minDate.current), 'dd');
        const minDateMonthNumber = format(new Date(minDate.current), 'MM');
        const minDateYearInt = format(new Date(minDate.current), 'yyyy');
        const minDateObject = new Date(minDateDayString + '-' + minDateMonthString + '-' + minDateYearString);
        
        if (isEqual(currentDateObject, maxDateObject)) {
            setIsRightButtonDisabled(true);
            setIsLeftButtonDisabled(false);
        }
        else if (isEqual(currentDateObject, minDateObject)) {
            setIsRightButtonDisabled(false);
            setIsLeftButtonDisabled(true);
        }
        else if (isAfter(currentDateObject, maxDateObject)) {
            setIsRightButtonDisabled(true);
            setIsLeftButtonDisabled(false);
            day.current = maxDateDayString;
            month.current = maxDateMonthString;
            monthNumber.current = parseInt(maxDateMonthNumber);
            year.current = maxDateYearString;
            setDateView();
        }
        else if (isBefore(currentDateObject, minDateObject)) {
            setIsRightButtonDisabled(false);
            setIsLeftButtonDisabled(true);
            day.current = minDateDayString;
            month.current = minDateMonthString;
            monthNumber.current = parseInt(minDateMonthNumber);
            year.current = minDateYearString;
            setDateView();
        }
        else {
            setIsRightButtonDisabled(false);
            setIsLeftButtonDisabled(false);
        }

    }

    //MARK:- Components
    const MonthAndYearDropDownComponent = () => {
        return (
            <View style={styles.dropDownContainerView}>
                <View style={styles.monthDropDownContainerView}>
                    <DropDownPicker
                        open={openMonthsDropDown}
                        value={monthValue}
                        items={months}
                        setOpen={setOpenMonthsDropDown}
                        setValue={setMonthValue}
                        setItems={setMonths}
                        listMessageContainerStyle={{
                            backgroundColor: 'grey'
                        }}
                        dropDownContainerStyle={{
                            backgroundColor: "#dfdfdf"
                        }}
                        listParentContainerStyle={{
                            justifyContent: "center",
                            alignItems: "center"
                        }}
                    />
                </View>
                <View style={styles.yearDropDownContainerView}>
                    <DropDownPicker
                        open={openYearsDropDown}
                        value={yearValue}
                        items={years}
                        setOpen={setOpenYearsDropDown}
                        setValue={setYearValue}
                        setItems={setYears}
                        listMessageContainerStyle={{
                            backgroundColor: 'grey'
                        }}
                        dropDownContainerStyle={{
                            backgroundColor: "#dfdfdf"
                        }}
                        listParentContainerStyle={{
                            justifyContent: "center",
                            alignItems: "center"
                        }}
                    />
                </View>
            </View>
        );
    }

    const MenuButtonComponent = () => {
        return (
            <TouchableOpacity activeOpacity={0.7} onPress={menuButtonAction} style={styles.menuIconTouchableOpacity}>
                <MaterialCommunityIcon name={"menu"} size={33} color={'#EAB543'} style={styles.addExpenseIncomeIcon} />
            </TouchableOpacity>
        );
    }

    const ViewTypeSegmentedControlComponent = () => {
        return (
            <SegmentedControl
                style={styles.viewTypeSegmentedControl}
                values={[TransactionViewType.day, TransactionViewType.week, TransactionViewType.month, TransactionViewType.year]}
                selectedIndex={viewTypeIndex}
                onChange={(event) => {
                    viewTypeSelectedAction(event.nativeEvent.selectedSegmentIndex);
                }}
                backgroundColor={'#444'}
                tintColor={'#EAB543'}
            />
        );
    }

    const ViewTypeSelectionComponent = () => {
        return (
            <View style={styles.viewTypeSelectionComponent}>
                <TouchableOpacity disabled={isLeftButtonDisabled} activeOpacity={isLeftButtonDisabled ? 1 : 0.3} onPress={leftButtonAction} style={isLeftButtonDisabled ? styles.leftChevronDisabledTouchableOpacity : styles.leftChevronEnabledTouchableOpacity}>
                    <MaterialCommunityIcon name={"chevron-left"} size={40} color={'#EAB543'} style={styles.addExpenseIncomeIcon} />
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.7} onPress={viewTypeTextButtonAction} style={styles.viewTypeDateOpacity}>
                    <Text style={styles.viewTypeDateText}>{dateViewString}</Text>
                </TouchableOpacity>
                <TouchableOpacity disabled={isRightButtonDisabled} activeOpacity={isRightButtonDisabled ? 1 : 0.5} onPress={rightButtonAction} style={isRightButtonDisabled ? styles.rightChevronDisabledTouchableOpacity : styles.rightChevronEnabledTouchableOpacity}>
                    <MaterialCommunityIcon name={"chevron-right"} size={40} color={'#EAB543'} style={styles.addExpenseIncomeIcon} />
                </TouchableOpacity>
            </View>
        );
    }

    const ExpenseIncometItemComponent = ({ item }) => {
        return (
            <View style={styles.sectionItemView}>
                <View style={styles.titleView}>
                    <Text style={styles.title}>{item.note}</Text>
                    <Text style={styles.category}>{item.category}</Text>
                </View>
                <View style={styles.amountView}>
                    {
                        (item.transactionType == TransactionType.expense) ?
                            <Text style={styles.expenseAmount}>- {item.amount}</Text> :
                            <Text style={styles.incomeAmount}>+ {item.amount}</Text>
                    }
                </View>
            </View>
        );
    }

    const ExpenseIncomeSectionHeaderView = ({ section }) => {
        return (
            <View style={styles.sectionHeaderView}>
                <Text style={styles.sectionHeaderTitle}>{section.title}</Text>
            </View>
        );
    }

    const ExpenseIncomeListComponent = () => {
        return (
            <SectionList
                sections={data}
                style={styles.list}
                renderItem={({ item }) => <ExpenseIncometItemComponent item={item} />}
                renderSectionHeader={ExpenseIncomeSectionHeaderView}
                stickySectionHeadersEnabled={false}
                showsVerticalScrollIndicator={false}
                keyExtractor={(item, index) => (item + index)}
            />
        )
    }

    const AddExpenseIncomeButtonComponent = () => {
        return (
            <TouchableOpacity activeOpacity={0.7} onPress={addExpenseIncomeButtonAction} style={styles.addExpenseIncomeTouchableOpacity}>
                <AntDesign name={"pluscircle"} size={50} color={'#EAB543'} style={styles.addExpenseIncomeIcon} />
            </TouchableOpacity>
        );
    }

    //MARK:- Button Actions
    const addExpenseIncomeButtonAction = () => {
        navigation.navigate('AddExpenseIncome');
    }

    const menuButtonAction = () => {
        navigation.toggleDrawer();
    }

    const viewTypeSelectedAction = (selectedIndex) => {
        setViewTypeIndex(selectedIndex);
        setDateView(selectedIndex);
    }

    const leftButtonAction = () => {
        setDateView(viewTypeIndex, DateViewTypeButtonAction.left);
    }

    const viewTypeTextButtonAction = () => {
        navigation.navigate('CalendarPicker');
    }

    const rightButtonAction = () => {
        setDateView(viewTypeIndex, DateViewTypeButtonAction.right);
    }

    return (
        <SafeAreaView style={styles.mainContentView}>
            {/* <MonthAndYearDropDownComponent /> */}
            <MenuButtonComponent />
            <ViewTypeSelectionComponent />
            <ViewTypeSegmentedControlComponent />
            <ExpenseIncomeListComponent />
            <AddExpenseIncomeButtonComponent />
        </SafeAreaView>
    );
}

export default HomeScreenView;