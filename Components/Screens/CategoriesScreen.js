import React, {
    useState,
    useEffect,
    useContext
} from "react";
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    FlatList
} from 'react-native';

import { useFocusEffect } from "@react-navigation/native";
import { SafeAreaView } from "react-native-safe-area-context";

import AntDesign from "react-native-vector-icons/AntDesign";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";

import LottieView from 'lottie-react-native';

import { UserContext } from "../../Contexts/UserContext";

import { collection, setDoc, addDoc, getDoc, getDocs, QuerySnapshot, QueryConstraint } from "firebase/firestore";
import { firebaseCloudFirestoreDB } from "../../Firebase/FirebaseAuthentication";

const CategoriesScreenView = ({ navigation, route }) => {

    const userData = useContext(UserContext);
    const [selectionMode, setSelectionMode] = useState(false);
    const [shouldShowActivityIndicator, setShouldShowActivityIndicator] = useState(true);
    const [categories, setCategories] = useState([]);

    const styles = StyleSheet.create({
        mainContentView: {
            flex: 1,
            backgroundColor: '#182C61',
            alignContent: 'center'
        },
        addCategoryButtonView: {
            marginBottom: '5%',
            alignSelf: 'center'
        },
        list: {
            top: -10
        },
        listItemView: {
            padding: 11,
            marginHorizontal: 0,
            backgroundColor: 'white',
            marginVertical: 0,
            borderRadius: 10,
            borderColor: 'gray',
            borderWidth: 0.3,
            marginTop: 0,
            flexDirection: 'row',
            justifyContent: 'space-between'

        },
        titleView: {
            justifyContent: 'center'
        },
        categoryImageView: {
            //marginTop: 25
        },
        title: {
            fontWeight: '600',
            fontSize: 17
        },
        sectionHeaderView: {
            padding: 12,
            justifyContent: 'center'
        },
        sectionHeaderTitle: {
            fontSize: 18,
            fontWeight: '700',
            color: '#fff',
            textAlign: 'left'
        },
        sectionItemView: {
            padding: 10,
            backgroundColor: 'rgba(233,233,195,0.7)',
            borderRadius: 17,
            borderColor: '#777',
            borderWidth: 0.6,
            marginLeft: 5,
            marginRight: 15,
            width: 200,
            marginVertical: 7

        },
        addCategoryTouchableOpacity: {
            right: '10%',
            bottom: '7%',
            position: 'absolute',
            alignItems: 'center',
            justifyContent: 'center',
            width: 50,
            height: 50
        },
        addCategoryIcon: {

        }
    });

    useEffect(() => {
        setSelectionMode(route.params.categorySelectionMode);
        getCategories();
    }, []);

    useFocusEffect(React.useCallback(() => {
        if (route.params?.categoryObject) {
            setCategories([...categories, route.params.categoryObject]);
        }
    }, [route.params?.categoryObject]));

    const getCategories = async () => {
        if (userData != null) {
            try {
                const querySnapshot = await getDocs(collection(firebaseCloudFirestoreDB, "users", userData.userData.uid, "categories"));
                setShouldShowActivityIndicator(false);
                if (!querySnapshot.empty) {
                    let categoriesArray = []
                    querySnapshot.forEach((doc) => {
                        categoriesArray.push(doc.data());
                    });
                    setCategories(categoriesArray);
                }
            }
            catch {
                alert(console.error.message);
            }
        }
        else {
            alert('There was some error in accessing your stored data. Please sign in again to authenticate.');
        }
    }

    const AddCategoryButtonComponent = () => {
        return (
            <TouchableOpacity>
                <View style={styles.addCategoryButtonView}>
                    <SimpleLineIcon name={'plus'} size={60} color={'#EAB543'} />
                </View>
            </TouchableOpacity>
        );
    }

    const CatergoryItemView = ({ item }) => {
        return (
            (selectionMode == true) ?
                <TouchableOpacity onPress={() => navigation.navigate('AddExpenseIncome', {
                    category: {
                        title: item.title,
                        image: item.image,
                        color: item.color
                    }
                })}>
                    <View style={styles.listItemView}>
                        <View style={styles.titleView}>
                            <Text style={styles.title}>{item.title}</Text>
                        </View>
                        <View style={styles.categoryImageView}>
                            <FontAwesome5Icon name={item.image} size={33} color={item.color} />
                        </View>
                    </View>
                </TouchableOpacity> :
                <View style={styles.listItemView}>
                    <View style={styles.titleView}>
                        <Text style={styles.title}>{item.title}</Text>
                    </View>
                    <View style={styles.categoryImageView}>
                        <FontAwesome5Icon name={item.image} size={33} color={item.color} />
                    </View>
                </View>
        );
    }

    const selectCategoryAction = () => {

    }

    const CategoryList = () => {
        return (
            <FlatList data={categories}
                renderItem={CatergoryItemView}
                keyExtractor={(item, index) => index.toString()}
                showsVerticalScrollIndicator={false}
                scrollsToTop={true}
                contentInsetAdjustmentBehavior={'always'}
                style={styles.list}
            />
        )
    }

    const addCategoryButtonAction = () => {
        navigation.navigate('AddCategory', {
            categories: categories
        });
    }

    return (
        <SafeAreaView style={styles.mainContentView}>
            <CategoryList />
            <TouchableOpacity activeOpacity={0.7} onPress={addCategoryButtonAction} style={styles.addCategoryTouchableOpacity}>
                <AntDesign name={"pluscircle"} size={50} color={'#EAB543'} style={styles.addCategoryIcon} />
            </TouchableOpacity>
            {(shouldShowActivityIndicator) ?
                <LottieView source={require('../../Assets/Loading-Spinner-Animation.json')} autoPlay loop colorFilters={[
                    {
                        keypath: 'Line',
                        color: '#EAB543',
                    },
                    {
                        keypath: 'Line transparent',
                        color: '#111',
                    },
                ]} />
                : null
            }
        </SafeAreaView>
    );
}

export default CategoriesScreenView;