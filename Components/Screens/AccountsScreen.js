import React, {
    useEffect,
    useState,
    useContext
} from "react";
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    FlatList
} from "react-native";

import { useFocusEffect } from "@react-navigation/native";

import { SafeAreaView } from "react-native-safe-area-context";

import AntDesign from "react-native-vector-icons/AntDesign";
import MaterialCommunityIcon from "react-native-vector-icons/MaterialCommunityIcons";

import LottieView from 'lottie-react-native';

import { UserContext } from "../../Contexts/UserContext";

import { collection, doc, setDoc, addDoc, getDoc, getDocs, runTransaction, query, where, serverTimestamp, updateDoc } from "firebase/firestore";
import { firebaseAuthentication, firebaseCloudFirestoreDB } from "../../Firebase/FirebaseAuthentication";

const AccountsScreenView = ({ navigation, route }) => {

    const userData = useContext(UserContext);
    const [accounts, setAccounts] = useState([]);
    const [shouldShowActivityIndicator, setShouldShowActivityIndicator] = useState(false);

    useEffect(() => {
        getAccounts();
    }, []);

    useFocusEffect(React.useCallback(() => {
        if (route.params?.accountObject) {
            setAccounts([...accounts, route.params.accountObject]);
        }
    }, [route.params?.accountObject]));

    const styles = StyleSheet.create({
        mainContentView: {
            flex: 1,
            backgroundColor: '#182C61',
            alignContent: 'center'
        },
        addCategoryButtonView: {
            marginBottom: '5%',
            alignSelf: 'center'
        },
        list: {
            top: -10
        },
        listItemView: {
            padding: '5%',
            marginHorizontal: '5%',
            backgroundColor: 'white',
            marginVertical: '5%',
            borderRadius: 10,
            borderColor: 'gray',
            borderWidth: 0.3,
            marginTop: 0,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',


        },
        titleView: {
            width: '90%',
            justifyContent: 'center'
        },
        categoryImageView: {
            //alignSelf: 'center'
            //marginTop: 25
        },
        title: {
            fontWeight: '600',
            fontSize: 17,
            marginBottom: '4%'
        },
        balance: {
            fontWeight: '600',
            fontSize: 22
        },
        sectionHeaderView: {
            padding: 12,
            justifyContent: 'center'
        },
        sectionHeaderTitle: {
            fontSize: 18,
            fontWeight: '700',
            color: '#fff',
            textAlign: 'left'
        },
        sectionItemView: {
            padding: 10,
            backgroundColor: 'rgba(233,233,195,0.7)',
            borderRadius: 17,
            borderColor: '#777',
            borderWidth: 0.6,
            marginLeft: 5,
            marginRight: 15,
            width: 200,
            marginVertical: 7

        },
        addCategoryTouchableOpacity: {
            right: '10%',
            bottom: '7%',
            position: 'absolute',
            alignItems: 'center',
            justifyContent: 'center',
            width: 50,
            height: 50
        },
        addCategoryIcon: {

        }
    });

    const getAccounts = async () => {
        if (userData != null) {
            try {
                const querySnapshot = await getDocs(collection(firebaseCloudFirestoreDB, "users", userData.userData.uid, "accounts"));
                setShouldShowActivityIndicator(false);
                if (!querySnapshot.empty) {
                    let accountsArray = []
                    querySnapshot.forEach((doc) => {
                        accountsArray.push(doc.data());
                    });
                    setAccounts(accountsArray);
                }
            }
            catch (error) {
                alert(error);
            }
        }
        else {
            alert('There was some error in accessing your stored data. Please sign in again to authenticate.');
        }
    }

    const AccountItemView = ({ item }) => {
        return (
            <View style={styles.listItemView}>
                <View style={styles.titleView}>
                    <Text style={styles.title}>{item.name}</Text>
                    <Text style={styles.balance}>{item.balance}</Text>
                </View>
                <TouchableOpacity onPress={() => selectAccountAction(item)}>
                    <View style={styles.categoryImageView}>
                        {(item.isSelected) ?
                            <MaterialCommunityIcon name={'checkbox-marked'} size={29} color={'#3B3B98'} /> :
                            <MaterialCommunityIcon name={'checkbox-blank'} size={29} color={'#3B3B98'} />
                        }
                    </View>
                </TouchableOpacity>
            </View>
        );
    }

    const selectAccountAction = async (currentSelectedAccount) => {
        if (userData != null) {
            try {
                const previousSelectedAccountsArray = accounts.filter((item) => {
                    return item.isSelected == true;
                });
                if (previousSelectedAccountsArray.length > 0) {
                    const previousSelectedAccount = previousSelectedAccountsArray[0];
                    if (currentSelectedAccount.name == previousSelectedAccount.name) {

                    }
                    else {
                        setShouldShowActivityIndicator(true);
                        console.log('previousSelectedAccount: ', previousSelectedAccount, 'currentSelectedAccount: ', currentSelectedAccount);

                        const previousSelectedAccountRef = doc(firebaseCloudFirestoreDB, "users", userData.userData.uid, "accounts", previousSelectedAccount.name);
                        const currentSelectedAccountRef = doc(firebaseCloudFirestoreDB, "users", userData.userData.uid, "accounts", currentSelectedAccount.name);

                        await updateDoc(previousSelectedAccountRef, {
                            isSelected: false
                        });
                        await updateDoc(currentSelectedAccountRef, {
                            isSelected: true
                        });

                        setShouldShowActivityIndicator(false);
                        getAccounts();
                    }
                }
                else {
                    alert('There was some error. Please try again.');
                }
            }
            catch (error) {
                setShouldShowActivityIndicator(false);
                alert(error);
            }
        }
        else {
            alert('There was some error in accessing your stored data. Please sign in again to authenticate.');
        }
    }

    const AccountsList = () => {
        return (
            <FlatList data={accounts}
                renderItem={AccountItemView}
                keyExtractor={(item, index) => index.toString()}
                showsVerticalScrollIndicator={false}
                scrollsToTop={true}
                contentInsetAdjustmentBehavior={'always'}
                style={styles.list}
            />
        )
    }

    const addAccountButtonAction = () => {
        navigation.push('AccountBalance', { isAddNewAccountMode: true, headerShown: true });
    }

    return (
        <SafeAreaView style={styles.mainContentView}>
            <AccountsList />
            <TouchableOpacity activeOpacity={0.7} onPress={addAccountButtonAction} style={styles.addCategoryTouchableOpacity}>
                <AntDesign name={"pluscircle"} size={50} color={'#EAB543'} style={styles.addCategoryIcon} />
            </TouchableOpacity>
            {(shouldShowActivityIndicator) ? <LottieView source={require('../../Assets/Loading-Spinner-Animation.json')} autoPlay loop colorFilters={[
                {
                    keypath: 'Line',
                    color: '#EAB543',
                },
                {
                    keypath: 'Line transparent',
                    color: '#111',
                },
            ]} /> : null}
        </SafeAreaView>
    );
}

export default AccountsScreenView;