import React, {
    useState,
    useEffect,
    Component,
} from "react";

import { Alert } from "react-native";

import { NavigationContainer } from '@react-navigation/native';

import { createNativeStackNavigator } from '@react-navigation/native-stack';
const StackNavigator = createNativeStackNavigator();

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
const TabNavigator = createBottomTabNavigator();

import { createDrawerNavigator, DrawerContentScrollView, DrawerItemList, DrawerItem } from '@react-navigation/drawer';
const DrawerNavigator = createDrawerNavigator();

import LottieView from 'lottie-react-native';

//Component Imports
import AuthSelectionScreenView from "../Screens/AuthSelectionScreenView";
import LoginSignUpScreenView from "../Screens/LoginSignUpScreen";
import WelcomeScreenView from "../Screens/WelcomeScreen";
import SetAccountBalanceScreenView from "../Screens/SetAccountBalanceScreen";
import HomeScreenView from "../Screens/HomeScreen";
import AccountsScreenView from "../Screens/AccountsScreen";
import CategoriesScreenView from "../Screens/CategoriesScreen";
import MyTransactionsScreenView from "../Screens/MyTransactionsScreen";
import AddExpenseIncomeScreenView from "../Screens/AddExpenseIncomeScreen";
import AddCategoryScreenView from "../Screens/AddCategoryScreen";
import LoadingScreenView from "../Screens/LoadingScreen";
import CalendarPickerScreenView from "../Screens/CalendarPickerScreen";

import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';

//Firebase
import { onAuthStateChanged, signOut } from "firebase/auth";
import { firebaseAuthentication } from "../../Firebase/FirebaseAuthentication";

//user Context
import { UserContext } from "../../Contexts/UserContext";

// const StackNavigatorComponent = ({ navigation }) => {
//     return (
//         <StackNavigator.Navigator screenOptions={{
//             headerStyle: {
//                 backgroundColor: '#EAB543'
//             },
//             headerTintColor: '#3B3B98',
//             headerTitleStyle: {
//                 fontWeight: '600',
//             },
//             gestureEnabled: false
//         }}>
//             <StackNavigator.Group>
//                 <StackNavigator.Screen name="AuthSelection" component={AuthSelectionScreenView} options={{
//                     headerShown: false,
//                     animation: 'slide_from_bottom'
//                 }} />
//                 <StackNavigator.Screen name="LoginSignUp" component={LoginSignUpScreenView} options={{
//                     headerShown: true,
//                     animation: 'slide_from_bottom',
//                     headerBackTitle: '',
//                     headerTitle: ''
//                 }} />
//                 <StackNavigator.Screen name="Welcome" component={WelcomeScreenView} options={{
//                     headerShown: false,
//                     animation: 'default',
//                     headerBackTitle: ''
//                 }} />
//                 <StackNavigator.Screen name="AccountBalance" component={SetAccountBalanceScreenView} options={{
//                     headerShown: false,
//                     animation: 'default',
//                     headerBackTitle: ''
//                 }} />
//                 <StackNavigator.Screen name="HomeDrawer" component={DrawerNavigatorComponent} options={{
//                     headerShown: false,
//                     animation: 'default',
//                     headerBackTitle: ''
//                 }} />
//                 <StackNavigator.Screen name="AddExpenseIncome" component={AddExpenseIncomeScreenView} options={{
//                     headerShown: true,
//                     animation: 'default',
//                     headerBackTitle: '',
//                     headerTitle: ''
//                 }} />
//                  <StackNavigator.Screen name="AddCategory" component={AddCategoryScreenView} options={{
//                     headerShown: true,
//                     animation: 'default',
//                     headerBackTitle: '',
//                     headerTitle: ''
//                 }} />
//             </StackNavigator.Group>
//         </StackNavigator.Navigator>
//     );
// }

// const DrawerNavigatorComponent = () => {
//     return (
//         <DrawerNavigator.Navigator initialRouteName="Home" screenOptions={{
//             headerStyle: {
//                 backgroundColor: '#EAB543'
//             },
//             headerTintColor: '#3B3B98',
//             headerTitleStyle: {
//                 fontWeight: '800',
//             },
//         }}
//             drawerContent={props => {
//                 return (
//                     <DrawerContentScrollView {...props}>
//                         <DrawerItemList {...props} />
//                         <DrawerItem label=""
//                             icon={({ focused, color, size }) => <MaterialCommunityIcon color={color} size={50} name={'logout'} />}
//                         />
//                     </DrawerContentScrollView>
//                 )
//             }}
//         >
//             <DrawerNavigator.Screen name="Home" component={HomeScreenView} options={{ title: "Dashboard" }} />
//             <DrawerNavigator.Screen name="Accounts" component={AccountsScreenView} options={{ title: "My Accounts" }} />
//             <DrawerNavigator.Screen name="Categories" component={CategoriesScreenView} options={{ title: "Categories" }} />
//             <DrawerNavigator.Screen name="MyTransactions" component={MyTransactionsScreenView} options={{ title: "My Transactions" }} />
//         </DrawerNavigator.Navigator>
//     );
// }

class NavigationController extends Component {
    static contextType = UserContext;
    constructor(props) {
        super(props)
        this.state = {
            shouldShowActivityIndicator: false
        }
    }

    componentDidMount() {
    }

    componentDidUpdate() {

    }

    componentWillUnmount() {
        this.setState = (state, callback) => {
            return;
        };
    }

    StackNavigatorComponent = ({ navigation }) => {
        return (
            <StackNavigator.Navigator screenOptions={{
                headerStyle: {
                    backgroundColor: '#EAB543'
                },
                headerTintColor: '#3B3B98',
                headerTitleStyle: {
                    fontWeight: '600',
                },
                gestureEnabled: false
            }}>
                {!(this.context.userData) ? (
                    <StackNavigator.Group>
                        <StackNavigator.Screen name="Loading" component={LoadingScreenView} options={{
                            headerShown: false,
                            animationTypeForReplace: (this.context.userData != null) ? 'push' : 'pop'
                        }} />
                        <StackNavigator.Screen name="AuthSelection" component={AuthSelectionScreenView} options={{
                            headerShown: false,
                            animationTypeForReplace: (this.context.userData != null) ? 'push' : 'pop'
                        }} />
                        <StackNavigator.Screen name="LoginSignUp" component={LoginSignUpScreenView} options={{
                            headerShown: true,
                            animation: 'slide_from_bottom',
                            headerBackTitle: '',
                            headerTitle: ''
                        }} />
                    </ StackNavigator.Group>
                ) : (
                    <StackNavigator.Group>
                        <StackNavigator.Screen name="Welcome" component={WelcomeScreenView} options={{
                            headerShown: false,
                            animation: 'default',
                            headerBackTitle: ''
                        }} />
                        <StackNavigator.Screen name="AccountBalance" component={SetAccountBalanceScreenView} options={{
                            headerShown: false,
                            animation: 'default',
                            headerBackTitle: ''
                        }} initialParams={{ isAddNewAccountMode: false, headerShown: false }} />
                        <StackNavigator.Screen name="HomeDrawer" component={this.DrawerNavigatorComponent} options={{
                            headerShown: false,
                            animation: 'default',
                            headerBackTitle: ''
                        }} />
                        <StackNavigator.Screen name="AddExpenseIncome" component={AddExpenseIncomeScreenView} options={{
                            headerShown: true,
                            animation: 'default',
                            headerBackTitle: '',
                            headerTitle: ''
                        }} />
                        <StackNavigator.Screen name="AddCategory" component={AddCategoryScreenView} options={{
                            headerShown: true,
                            animation: 'default',
                            headerBackTitle: '',
                            headerTitle: ''
                        }} />
                        <StackNavigator.Screen name="Categories" component={CategoriesScreenView} options={{
                            headerShown: true,
                            animation: 'default',
                            headerBackTitle: '',
                            headerTitle: ''
                        }} />
                         <StackNavigator.Screen name="CalendarPicker" component={CalendarPickerScreenView} options={{
                                headerShown: true,
                                headerBackTitle: '',
                                animation: 'slide_from_bottom',
                                headerTitle: 'Select Date',
                                headerBackVisible: false
                            }} />
                    </StackNavigator.Group>
                )}
            </StackNavigator.Navigator>
        );
    }

    DrawerNavigatorComponent = () => {
        return (
            <DrawerNavigator.Navigator initialRouteName="Home" screenOptions={{
                headerStyle: {
                    backgroundColor: '#EAB543'
                },
                headerTintColor: '#3B3B98',
                headerTitleStyle: {
                    fontWeight: '800',
                },
                drawerActiveTintColor: '#EAB543',
                drawerInactiveTintColor: 'gray',
                drawerItemStyle: {

                },
                drawerLabelStyle: {
                    fontStyle: 'normal',
                    fontWeight: 'bold',
                    fontSize: 17
                },
                drawerContentContainerStyle: {

                },
                drawerStyle: {
                    backgroundColor: '#3B3B98'
                },
                drawerType: 'back',
                drawerHideStatusBarOnOpen: true,
                drawerStatusBarAnimation: 'fade'
            }}
                drawerContent={props => {
                    return (
                        <DrawerContentScrollView {...props}>
                            <DrawerItemList {...props} />
                            <DrawerItem label=""
                                icon={({ focused, color, size }) => <MaterialCommunityIcon color={'#EAB543'} size={50} name={'logout'} />}
                                onPress={this.showLogoutConfirmationAlert}
                            />
                        </DrawerContentScrollView>
                    )
                }}
            >
                <DrawerNavigator.Screen name="Home" component={HomeScreenView} options={{ title: "Dashboard", headerShown: false, }} />
                <DrawerNavigator.Screen name="Accounts" component={AccountsScreenView} options={{ title: "My Accounts" }} />
                <DrawerNavigator.Screen name="Categories" component={CategoriesScreenView} options={{ title: "Categories" }} initialParams={{ categorySelectionMode: false }} />
                <DrawerNavigator.Screen name="MyTransactions" component={MyTransactionsScreenView} options={{ title: "My Transactions" }} />
            </DrawerNavigator.Navigator>
        );
    }

    showLogoutConfirmationAlert = () => {
        Alert.alert(
            "Confirmation",
            "Are you sure you want to logout?",
            [
                {
                    text: "No",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                { text: "Yes", onPress: this.logoutAction }
            ]
        );
    }

    logoutAction = () => {
        this.setState({
            shouldShowActivityIndicator: true
        });
        signOut(firebaseAuthentication).then(() => {
            this.setState({
                shouldShowActivityIndicator: false
            });
        }).catch((error) => {
            this.setState({
                shouldShowActivityIndicator: false
            });
            alert(error.message);
        });

    }

    render() {
        return (
            <NavigationContainer>
                <this.StackNavigatorComponent />
                {(this.state.shouldShowActivityIndicator) ? <LottieView source={require('../../Assets/Loading-Spinner-Animation.json')} autoPlay loop colorFilters={[
                    {
                        keypath: 'Line',
                        color: '#EAB543',
                    },
                    {
                        keypath: 'Line transparent',
                        color: '#111',
                    },
                ]} /> : null}
            </NavigationContainer>
        );
    }
}

export default NavigationController;